// ==UserScript==
// @name         pks.we-think fix
// @namespace    https://gitlab.com/bilut1310/
// @version      0.3
// @description  威盛保經管理系統 - 現代瀏覽器支援
// @author       Po-Chin SHen
// @include      /https?://pks\.we-think\.com.tw/
// @homepage     https://gitlab.com/bilut1310/pks_we-think_fix
// @supportURL   https://gitlab.com/bilut1310/pks_we-think_fix/issues
// @updateURL    https://gitlab.com/bilut1310/pks_we-think_fix/raw/master/pks_we-think_fix.user.js
// @downloadURL  https://gitlab.com/bilut1310/pks_we-think_fix/raw/master/pks_we-think_fix.user.js
// @grant        none
// ==/UserScript==

(function main() {
    // ref: https://stackoverflow.com/questions/25663053/how-can-i-make-window-showmodaldialog-work-in-chrome-37
    if (!window.showModalDialog) {
        window.showModalDialog = function (uri, arguments, options) {
            let w;
            let h;
            let resizable = 'no';
            let scroll = 'no';
            let status = 'no';

            // get the modal specs
            for (const mdattr of options.split(';')) {
                let [n, v] = mdattr.split(':');

                if (n) { n = n.trim().toLowerCase(); }
                if (v) { v = v.trim().toLowerCase(); }

                if (n === 'dialogheight') {
                    h = v.replace('px', '');
                } else if (n === 'dialogwidth') {
                    w = v.replace('px', '');
                } else if (n === 'resizable') {
                    resizable = v;
                } else if (n === 'scroll') {
                    scroll = v;
                } else if (n === 'status') {
                    status = v;
                }
            }

            const left = window.screenX + (window.outerWidth / 2) - (w / 2);
            const top = window.screenY + (window.outerHeight / 2) - (h / 2);
            const target = window.open(uri, uri, `toolbar=no,location=no,directories=no,status=${status},menubar=no,scrollbars=${scroll},resizable=${resizable},copyhistory=no,width=${w},height=${h},top=${top},left=${left}`);
            target.focus();
        };
    }

    document.onclick = function deleteConfirm(event) {
        const text = event.target.innerHTML;
        if (text === '刪除' || text === 'Delete' || text === '登出') {
            return confirm(`確定要${text}嗎?`);
        }
    };
})();
